export default [
{
	"name": "家居安装",
	"foods": [{
			"name": "床体安装",
			"key": "床体安装",
			"icon": "/static/img/category/anzhuan/chuangti.png",
			"cat": 6
		},
					{
			"name": "柜类安装",
			"key": "柜类安装",
			"icon": "/static/img/category/anzhuan/guilei.png",
			"cat": 6
		},
					{
			"name": "沙发安装",
			"key": "沙发安装",
			"icon": "/static/img/category/anzhuan/shafa.png",
			"cat": 6
		},
					{
			"name": "桌椅安装",
			"key": "桌椅安装",
			"icon": "/static/img/category/anzhuan/zhuoyi.png",
			"cat": 6
		},
				   {
			"name": "茶几安装",
			"key": "茶几安装",
			"icon": "/static/img/category/anzhuan/chaji.png",
			"cat": 6
		},
					{
			"name": "台案屏风",
			"key": "台案屏风",
			"icon": "/static/img/category/anzhuan/taian.png",
			"cat": 6
		},
	]
},
{
	"name": "电气安装",
	"foods": [{
			"name": "空调拆装",
			"key": "空调拆装",
			"icon": "/static/img/category/dianqi/kongtiaocz.png",
			"cat": 3
		},
					 {
			"name": "油烟机安装",
			"key": "油烟机安装",
			"icon": "/static/img/category/dianqi/youyanjiaz.png",
			"cat": 3
		},
					 {
			"name": "燃气灶安装",
			"key": "燃气灶安装",
			"icon": "/static/img/category/dianqi/ranjizaoaz.png",
			"cat": 3
		},
					 {
			"name": "其它电气安装",
			"key": "其它电气安装",
			"icon": "/static/img/category/dianqi/qitadianqi.png",
			"cat": 3
		},
	]
},
{
		"name": "灯具安装",
		"foods": [{
				"name": "灯具安装",
				"key": "灯具安装",
				"icon": "/static/img/category/dengju/dengjuaz.png",
				"cat": 3
			},
						 {
				"name": "开关插座安装",
				"key": "开关插座安装",
				"icon": "/static/img/category/dengju/kaiguanchz.png",
				"cat": 3
			},
						 {
				"name": "电路铺设改造",
				"key": "电路铺设改造",
				"icon": "/static/img/category/dengju/dianlugz.png",
				"cat": 3
			},
		]
	},
	{
		"name": "家电维修",
		"foods": [{
				"name": "常用家电",
				"key": "常用家电",
				"icon": "/static/img/category/jiadian/changyongjd.png",
				"cat": 3
			},
						 {
				"name": "厨房家电",
				"key": "厨房家电",
				"icon": "/static/img/category/jiadian/chufangjd.png",
				"cat": 3
			},
						 {
				"name": "生活家电",
				"key": "生活家电",
				"icon": "/static/img/category/jiadian/shenghuojd.png",
				"cat": 3
			},
						 {
				"name": "制冷家用设备",
				"key": "制冷家用设备",
				"icon": "/static/img/category/jiadian/zhilengsb.png",
				"cat": 3
			},
		]
	},
	{
		"name": "家居维保",
		"foods": [{
				"name": "家具维修",
				"key": "家具维修",
				"icon": "/static/img/category/weibao/jiajuwx.png",
				"cat": 3
			},
						 {
				"name": "家具保养",
				"key": "家居保养",
				"icon": "/static/img/category/weibao/jiajuby.png",
				"cat": 3
			},
						 {
				"name": "家居清洗",
				"key": "家居清洗",
				"icon": "/static/img/category/weibao/jiajuqx.png",
				"cat": 3
			},
						 
		]
	}
]
